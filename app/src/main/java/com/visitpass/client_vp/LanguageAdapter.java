package com.visitpass.client_vp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> {

    private List<LanguageRecycler> mData;
    private List<String> icons;
    private Context context;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    ProgressDialog dialog;
    private TextureView img;

    // data is passed into the constructor
    LanguageAdapter(Context context, List<LanguageRecycler> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context=context;
    }

    public LanguageAdapter(Context context, Object o) {
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_recyler_language, parent, false);
        return new ViewHolder(view);
    }

    // тут пишем нащи вставные данные
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LanguageRecycler language = mData.get(position);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        holder.myTextView.setText(language.getName());
        //Берем язык с дефолтного положения
        String languages=SharedPrefManager.getmInstance(context).getSelectedLanguage();

        if(language.getTag().equals(languages)) {
            //Log.d("Daasd","Yeap there is no work!!");
            holder.myTextView.setEnabled(false);
            holder.myTextView.setClickable(false);
            holder.myTextView.setCompoundDrawablePadding((int) convertDpToPixel(60,context));
            holder.myTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_box_black_24dp, 0);
            holder.myTextView.setTextColor(Color.parseColor("#BB133E"));
            if (Build.VERSION.SDK_INT > 15) {
                holder.myTextView.setBackgroundResource(R.drawable.back_city_red);
            }
        }
        Log.d("Daasd",language.getIcon());
        //для работы с картинками

        Glide.with(context).load(language.getIcon()).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.logo_small).into(holder.img);

    }


    // количество строк
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ImageView img;
        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.goodText);
            img = itemView.findViewById(R.id.img);
           itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            dialog= new ProgressDialog(context);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("Please wait ...");
            dialog.setCancelable(false);
            dialog.show();

            // check if item still exists
            if(pos != RecyclerView.NO_POSITION){
                LanguageRecycler clickedDataItem = mData.get(pos);
                SharedPrefManager.getmInstance(context).setLanguage(clickedDataItem.getTag());
                Toast.makeText(view.getContext(), "You clicked " + clickedDataItem.getTag(), Toast.LENGTH_SHORT).show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((Activity)context).finish();
                        context.startActivity(new Intent(context,MainActivity.class));
                        ((Activity)context).overridePendingTransition(0, 0);
                    }
                }, 1000);


            }
        }
    }
    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
