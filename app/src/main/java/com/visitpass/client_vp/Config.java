package com.visitpass.client_vp;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Config {

    public static String url_lang_image="https://visitpass.com/storage/";
    public static String url_image="https://visitpass.com/storage/";

    public static void finishApp(Context context){
        ((Activity)context).finish();
    }

    public static void refreshApp(Context context){
        ((Activity)context).recreate();
    }


    public static List<AlbumAttractions> parseJsonAttractions(Context context,int category_id) {
        List<AlbumAttractions> albumList;
        AlbumAttractions a;
        albumList = new ArrayList<>();

        String attraction = SharedPrefManager.getmInstance(context).getKey("key_attractions");

        if(attraction!=null) {

            try {
                JSONArray jsonArray = new JSONArray(attraction);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    int value1 = jsonObject1.optInt("category_id");
                    int value2 = jsonObject1.optInt("attraction_id");
                    String value3 = jsonObject1.optString("normaly_price_child");
                    String value4 = jsonObject1.optString("normaly_price_adult");
                    String value5 = jsonObject1.optString("info");
                    String value6 = jsonObject1.optString("name");
                    String value7 = jsonObject1.optString("address");
                    String value8 = jsonObject1.optString("map_label");
                    String value9 = jsonObject1.optString("map_lat");
                    String value10 = jsonObject1.optString("map_lng");


                    if (value1 == category_id) {
                        String picture = null;
                        if (jsonObject1.has("picture")) {
                            SharedPrefManager.getmInstance(context).setKey("pics_all",  jsonObject1.get("picture").toString());
                                JSONObject jsonObject2 = jsonObject1.getJSONArray("picture").getJSONObject(0);
                                picture = url_image + jsonObject2.optString("pict_src");
                        }else picture = "https://media-cdn.tripadvisor.com/media/photo-s/12/4b/ee/15/photo0jpg.jpg";

                        //Log.d("CheckPicture", !TextUtils.isEmpty(picture) ? picture : i + "No picture");
                        //Log.d("Category", String.valueOf(category_id));
                        
                        a = new AlbumAttractions(picture, value1, value2, value4, value3, value6, "$" + value4, value5, value7, value8, value9, value10);
                        albumList.add(a);
                    }
                    //System.out.println(picture + " " + value2);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(context, "Please check if you have internet connection? If not, enable and run the application again.", Toast.LENGTH_SHORT).show();
        }

        return albumList;
    }

    public static List<CategoryAlbum> parseJsonCategory(Context context) {

        List<CategoryAlbum> albumList;
        CategoryAlbum a;
        albumList = new ArrayList<>();


        String attraction = SharedPrefManager.getmInstance(context).getKey("key_category");
        if(attraction!=null) {
            try {
                JSONArray jsonArray = new JSONArray(attraction);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    int value1 = jsonObject1.optInt("id");
                    String value2 = jsonObject1.optString("img_mob_path");
                    String value3 = jsonObject1.optString("name");

                    a = new CategoryAlbum(value1, "https://visitpass.com/visitpass/"+value2, value3);
                    albumList.add(a);
                    //System.out.println(value1 + " " + value2);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Toast.makeText(context, "Please check if you have internet connection? If not, enable and run the application again.", Toast.LENGTH_SHORT).show();
        }
        return albumList;
    }

    public static List<LanguageRecycler> parseJsonLanguage(Context context) {

        List<LanguageRecycler> albumList;
        LanguageRecycler a;
        albumList = new ArrayList<>();

        String attraction = SharedPrefManager.getmInstance(context).getKey("key_language");
        if(attraction!=null){
        try {
            JSONArray jsonArray = new JSONArray(attraction);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = url_lang_image + jsonObject1.optString("icon_path");
                String value2 = jsonObject1.optString("alias");
                String value3 = jsonObject1.optString("language");
                Log.d("TAFGA",value1);
                a = new LanguageRecycler(value3,value1,value2);
                albumList.add(a);
                //System.out.println(value1 + " " + value2);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        }else {
            a = new LanguageRecycler("", "", "");
            albumList.add(a);
            Toast.makeText(context, "Please check if you have internet connection? If not, enable and run the application again.", Toast.LENGTH_SHORT).show();
        }

        return albumList;
    }

    public static List<LanguageRecycler> parseJsonCity(Context context) {

        List<LanguageRecycler> albumList;
        LanguageRecycler a;
        albumList = new ArrayList<>();

        String attraction = SharedPrefManager.getmInstance(context).getKey("key_city");
        if(attraction!=null) {
            try {
                JSONArray jsonArray = new JSONArray(attraction);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    int value1 = jsonObject1.optInt("id");
                    String value2 = jsonObject1.optString("name");

                    a = new LanguageRecycler(value2, "new_york", value1);
                    albumList.add(a);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            a = new LanguageRecycler("", "", "");
            albumList.add(a);
            Toast.makeText(context, "Please check if you have internet connection? If not, enable and run the application again.", Toast.LENGTH_SHORT).show();
        }

        return albumList;
    }

    public boolean setDataSinglePage(Context context,View view){



        return false;
    }

    public static String[] getInfoFromProduct(Context context,String name,Integer part){
        String boo = null;
        String[] value = new String[0];
        String albumList = null;

        String product = SharedPrefManager.getmInstance(context).getKey("key_product");
        //Log.d("LENDASDSAd", String.valueOf(product));
        try{

            JSONArray jsonArray = new JSONArray(product);
           Log.d("dasdas", String.valueOf(jsonArray.length()));
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                Integer part_id =jsonObject1.getInt("part_id");
                if(part_id.equals(part)) {
                    Log.d("Proverka", String.valueOf(part_id.equals(part)));
                    value[i] = jsonObject1.optString(name);

                }
            }
            Log.d("dasdas223123", String.valueOf(value.length));
        }catch (JSONException e){
            e.printStackTrace();
        }


        return value;
    }

}
