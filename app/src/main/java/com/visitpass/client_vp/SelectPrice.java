package com.visitpass.client_vp;

public class SelectPrice {
    private String title;
    private CharSequence a_price;
    private CharSequence c_price;

    public SelectPrice(){

    }

    public SelectPrice(String title,CharSequence adult_price,CharSequence child_price){
        this.title=title;
        this.a_price=adult_price;
        this.c_price=child_price;
    }

    public String getTitle(){
        return this.title;
    }

    public CharSequence getAdult(){
        return this.a_price;
    }

    public CharSequence getChild(){
        return this.c_price;
    }
}
