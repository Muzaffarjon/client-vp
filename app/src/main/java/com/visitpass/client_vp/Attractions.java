package com.visitpass.client_vp;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Belal on 18/09/16.
 */


public class Attractions extends Fragment {

    private RecyclerView recyclerView;
    private CategoryAlbumAdapter adapter;
    public ArrayList<CategoryAlbum> dataItems;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.attractions_category, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Category");


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        //Log.d("EXTRA_ATTRACTION_ID", SharedPrefManager.getmInstance(getActivity()).getKey("key_attractions"));
        if(SharedPrefManager.getmInstance(getActivity()).getKey("key_category")!=null){
            dataItems= (ArrayList<CategoryAlbum>) Config.parseJsonCategory(getActivity());
            adapter = new CategoryAlbumAdapter(getActivity(), dataItems);
            adapter.setHasStableIds(true);

            if (this.getContext().getResources().getConfiguration().orientation ==
                    this.getContext().getResources().getConfiguration()
                            .ORIENTATION_LANDSCAPE) {
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(5), false));
            } else {
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));

            }

            //recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(20);
            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getActivity(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                        @Override public void onItemClick(View view, int position) {

                            //Log.d("ClickedCategory", String.valueOf(position));
                            //Log.d("GetItemFromData", dataItems.get(position).getName());

                            Intent intent = new Intent(getActivity(), AttractionActivity.class);

                            SharedPrefManager.getmInstance(getContext()).setTitle(dataItems.get(position).getName());

                            SharedPrefManager.getmInstance(getContext()).setKey(String.valueOf(dataItems.get(position).getId()),"key_update");
                            startActivity(intent);
                        }

                        @Override public void onLongItemClick(View view, int position) {
                            // do whatever
                        }
                    })
            );
            //Log.d("EXTRA_CATEGORY_ID", SharedPrefManager.getmInstance(getContext()).getKey("key_update"));
            adapter.notifyDataSetChanged();
        }

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
