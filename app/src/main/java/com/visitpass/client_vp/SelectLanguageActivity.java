package com.visitpass.client_vp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

public class SelectLanguageActivity extends AppCompatActivity {

    private LanguageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LinearLayout linearLayout= (LinearLayout) findViewById(R.id.city_form);
        List<LanguageRecycler> array = Config.parseJsonLanguage(SelectLanguageActivity.this);
        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvAnimals);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //Log.d("TAFGA", String.valueOf(array));
        adapter = new LanguageAdapter(this, array);
        if(adapter!=null)
        recyclerView.setAdapter(adapter);



    }

    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(SelectLanguageActivity.this, MainActivity.class);
        startActivity(intent);
        return true;
    }
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked " + position, Toast.LENGTH_SHORT).show();
    }

}

//Log.d("TAF",SharedPrefManager.getmInstance(this).getKey("key_language"));

        /*ArrayList<LanguageRecycler> animalNames = new ArrayList<>();

        LanguageRecycler lang= new LanguageRecycler("English","http://www.picshare.ru/uploads/190215/caaqBA7OG6.png","en");
        animalNames.add(lang);

        lang=new LanguageRecycler("Português","http://www.picshare.ru/uploads/190215/jU8hxj6CjV.png","pt");
        animalNames.add(lang);

        lang=new LanguageRecycler("Español","http://www.picshare.ru/uploads/190215/jU8hxj6CjV.png","es");
        animalNames.add(lang);

        lang=new LanguageRecycler("Deutsch","http://www.picshare.ru/uploads/190215/jU8hxj6CjV.png","de");
        animalNames.add(lang);

        lang=new LanguageRecycler("Français","http://www.picshare.ru/uploads/190215/caaqBA7OG6.png","fr");
        animalNames.add(lang);

        lang=new LanguageRecycler("Italiano","http://www.picshare.ru/uploads/190215/jU8hxj6CjV.png","it");
        animalNames.add(lang);

        lang=new LanguageRecycler("中文","http://www.picshare.ru/uploads/190215/caaqBA7OG6.png","zh");
        animalNames.add(lang);
        */
        /*final int margins=R.dimen.margin_bottom_of_textview;
        final int height=R.dimen.height_of_textview;

        TextView[] btn = new TextView[5];
        for(int i = 0; i < 5; i++) {
            btn[i] = new TextView(this);
            btn[i].setId(i);
            btn[i].setPadding(10,10,10,10);
            btn[i].setHeight(height);
            btn[i].setTextSize(20);
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            llp.setMargins(0, 0, 0, margins); // llp.setMargins(left, top, right, bottom);
            btn[i].setLayoutParams(llp);
            btn[i].setBackgroundResource(R.drawable.custom_edit_text);
            if(Build.VERSION.SDK_INT > 17) {
                btn[i].setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.english,0,0,0);
            }
            btn[i].setText("dynamic TextView " + i);
            linearLayout.addView(btn[i]);
            btn[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Snackbar.make(findViewById(R.id.relatived),"You have Clicked",Snackbar.LENGTH_LONG).show();
                }
            });
        }*/