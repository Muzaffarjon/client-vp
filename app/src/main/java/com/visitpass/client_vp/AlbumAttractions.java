package com.visitpass.client_vp;

public class AlbumAttractions {

    private String thumbnail;
    private int category_id;
    private int attraction_id;
    private String normally_price_child;
    private String normally_price_adult;
    private String name;
    private String price;
    private String info;
    private String address;
    private String map_lebel;
    private String map_lat;
    private String map_lng;


    public AlbumAttractions() {
    }

    public AlbumAttractions(String thumbnail, int category, int attraction_id, String normally_price_adult, String normally_price_child,
                            String title, String price, String info, String address, String map_lebel, String map_lat, String map_lng) {
        this.thumbnail = thumbnail;
        this.category_id = category;
        this.attraction_id = attraction_id;
        this.normally_price_adult = normally_price_adult;
        this.normally_price_child = normally_price_child;
        this.name = title;
        this.price = price;
        this.info = info;
        this.address = address;
        this.map_lebel = map_lebel;
        this.map_lat = map_lat;
        this.map_lng = map_lng;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getInfo() {
        return info;
    }

    public String getAddress() {
        return address;
    }

    public String getMaplng() {
        return map_lng;
    }

    public int getCategoryId() {
        return category_id;
    }

    public int getAttractionId() {
        return attraction_id;
    }

    public String getNormallyPriceChild() {
        return normally_price_child;
    }

    public String getNormallyPriceAdult() {
        return normally_price_adult;
    }

    public String getMapLebel() {
        return map_lebel;
    }

    public String getMapLat() {
        return map_lat;
    }

}
