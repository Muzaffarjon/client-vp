package com.visitpass.client_vp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    NavigationView navigationView;
    MenuItem menu;
    Handler mainHandler = new Handler(Looper.getMainLooper());
    RelativeLayout mProgressView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeLanguage();
        setContentView(R.layout.activity_main);
        //Log.d("Default Language",Locale.getDefault().getLanguage());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //mProgressView=(RelativeLayout) findViewById(R.id.progress_bar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //setNavMenuItemThemeColors(202020);
        //prepareAlbums();

        //if(SharedPrefManager.getmInstance(this).getKey("key_category")==null)


        if(SharedPrefManager.getmInstance(this).getSelectedCity()==null)
            SharedPrefManager.getmInstance(this).setCity("new_york");

        //Log.d("MemBoom",SharedPrefManager.getmInstance(this).getKey("key_update"));

        //mProgressView.setVisibility(View.GONE);
        displaySelectedScreen(R.id.attraction);


        hideItem();
    }

    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //Log.d("DASDA RESUME", "MainActivity: onResume()");
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void hideItem() {
        Menu nav_Menu = navigationView.getMenu();
        if (SharedPrefManager.getmInstance(this).isLoggedIn() == true) {
            nav_Menu.findItem(R.id.login).setVisible(false);
            nav_Menu.findItem(R.id.profile).setVisible(true);
            nav_Menu.findItem(R.id.profile).setTitle(SharedPrefManager.getmInstance(this).getEmail());
            nav_Menu.findItem(R.id.log_out).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.login).setVisible(true);
            nav_Menu.findItem(R.id.profile).setVisible(false);
            nav_Menu.findItem(R.id.log_out).setVisible(false);
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/


    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.login:
                //finish();
                //overridePendingTransition( R.anim.slide_out_left, R.anim.slide_in_right);
                Intent star = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(star);
               // overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_left);

                break;
            case R.id.map:
                overridePendingTransition(0, 0);
                startActivity(new Intent(MainActivity.this,MapsActivity.class));

                break;
            case R.id.attraction:
                fragment = new Attractions();
                break;
            case R.id.buy:
                overridePendingTransition(0, 0);
                startActivity(new Intent(MainActivity.this,Step1Activity.class));
                break;
            case R.id.useful:
                overridePendingTransition(R.anim.enter, R.anim.exit);
                fragment = new Useful();
                break;
            case R.id.select_city:

                //overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(MainActivity.this,SelectActivity.class));
                //finish();
                break;
            case R.id.language:
                //finish();
                //overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(MainActivity.this,SelectLanguageActivity.class));
                //finish();
                break;
            case R.id.site:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://visitpass.com"));
                startActivity(browserIntent);
                break;

            case R.id.log_out:
                SharedPrefManager.getmInstance(MainActivity.this).logout();
                finish();
                overridePendingTransition( 0, 0);
                startActivity(getIntent());
                overridePendingTransition( 0, 0);
                break;

        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.gandash, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //calling the method displayselectedscreen and passing the id of selected menu
        displaySelectedScreen(item.getItemId());
        //make this method blank
        return true;
    }

    public void changeLanguage(){

        if(SharedPrefManager.getmInstance(this).getSelectedLanguage()==null)
            SharedPrefManager.getmInstance(this).setLanguage("en");

        String language=SharedPrefManager.getmInstance(this).getSelectedLanguage();

        Locale myLocale = new Locale(language);

        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

    }
}
