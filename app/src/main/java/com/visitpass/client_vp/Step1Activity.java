package com.visitpass.client_vp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.visitpass.client_vp.mRecycler.TVShowFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Step1Activity extends AppCompatActivity implements ClickInterface {

    String http = "https://visitpass.com/getProduct";
    String cy_id;

    ProgressDialog dialog;

    String [] SPINNERLIST={"NO","YES"};
    TextView pass_duration;
    AlertDialog.Builder builder;
    Spinner adult,child,insure;
    RelativeLayout raw_insurance,raw_discount;
    TextView total_price,total_discount,total_insurance;
    Double insurance_percent=4.00,price, discount,summ,insurance_total,insurance_percent_from;

    Button st_button1,st_button2;


    private SelectPriceAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        dialog= new ProgressDialog(Step1Activity.this);

        dialog.setMessage("Please wait ...");

        dialog.show();

        final ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(Step1Activity.this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter2.add("188.00");
        arrayAdapter2.add("250.00");
        arrayAdapter2.add("300.00");
        arrayAdapter2.add("180.00");
        arrayAdapter2.add("75.00");

        adult=findViewById(R.id.adult_count);
        child=findViewById(R.id.child_count);
        insure=findViewById(R.id.insurance);

        total_discount=findViewById(R.id.total_discount);
        total_insurance=findViewById(R.id.total_insurance);
        total_price=findViewById(R.id.total_price);


        pass_duration=findViewById(R.id.pass_duration);
        raw_insurance=findViewById(R.id.raw_insurance);
        raw_discount=findViewById(R.id.raw_discount);

        pass_duration.setText(arrayAdapter2.getItem(0));

        st_button1=findViewById(R.id.st_button1);
        st_button2=findViewById(R.id.st_button2);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.st_button1:
                        pass_duration.setText(arrayAdapter2.getItem(0));
                        st_button1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        st_button2.setBackgroundColor(getResources().getColor(R.color.Primary));
                        summAll();
                        break;
                    case R.id.st_button2:
                        pass_duration.setText(arrayAdapter2.getItem(1));
                        st_button2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                        st_button1.setBackgroundColor(getResources().getColor(R.color.Primary));
                        summAll();
                        break;
                }

            }
        };

        st_button1.setOnClickListener(onClickListener);
        st_button2.setOnClickListener(onClickListener);
        builder = new AlertDialog.Builder(this);
        List<SelectPrice> price=new ArrayList<SelectPrice>();
        SelectPrice a = new SelectPrice();
        //a = new SelectPrice("Test",Float.valueOf("43.50"),Float.valueOf("43.50"));
        //price.add(a);
        //a = new SelectPrice("Test 2",Float.valueOf("45.50"),Float.valueOf("45.50"));
        //price.add(a);

        final FragmentManager fm=getSupportFragmentManager();
        final TVShowFragment tv=new TVShowFragment();
        adapter= new SelectPriceAdapter(this,price);

        pass_duration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv.show(fm,"TV_tag");

//                AlertDialog.Builder builderSingle = new AlertDialog.Builder(Step1Activity.this);
//                builderSingle.setIcon(R.drawable.ic_launcher_background);
//                builderSingle.setTitle("Select One Name:-");
//
//
//                builderSingle.setAdapter(arrayAdapter2, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String strName = arrayAdapter2.getItem(which);
//                        pass_duration.setText(strName);
//                        summAll();
//                    }
//                });
//                builderSingle.show();
//                AlertDialog.Builder builder = new AlertDialog.Builder(Step1Activity.this);
//                builder.setTitle("Select");
//                builder.setAdapter((SelectPriceAdapter) adapter,
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int item) {
//                                //Toast.makeText(Step1Activity.this, "You selected: " + a[item],Toast.LENGTH_LONG).show();
//                                dialog.dismiss();
//                            }
//                        });
//                AlertDialog alert = builder.create();
//                alert.show();
            }
        });

        Button step1=(Button) findViewById(R.id.step1_button);

        getInsurance();
        getAdultCount();
        getChildCount();

//        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,SPINNERLIST);
//        MaterialBetterSpinner betterSpinner=findViewById(R.id.insurance);
//        betterSpinner.setBackgroundResource(R.drawable.custom_edit_text);
//        betterSpinner.setAdapter(arrayAdapter);
//        betterSpinner.setText(SPINNERLIST[0]);


        step1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Step1Activity.this,BeforeActivity.class));
            }
        });

        /* http request */
        sendHttpRequest();

        //Log.d("dasdas", String.valueOf(Config.getInfoFromProduct(Step1Activity.this,"name")));
    }


    private void sendHttpRequest(){

        OkHttpClient client = new OkHttpClient();

        String city=SharedPrefManager.getmInstance(this).getKey("key_city");
       //Log.d("TEAGGGASD",city);
        try{
            JSONArray jsonObject = new JSONArray(city);
            JSONObject jsonObject1 = jsonObject.getJSONObject(0);
            cy_id=jsonObject1.getString("id");
        }catch (JSONException e){
            e.printStackTrace();
        }



        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("city_id", String.valueOf(cy_id))
                .build();

        Request request = new Request.Builder().url(http).post(requestBody).build();

        client.newCall(request).enqueue(new Callback() {
            String myResponse;
            @Override
            public void onFailure(Call call, IOException e) {

                Log.d("TAFFFFF", e.toString()); // no need inside run()

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                myResponse = response.body().string();
                //Log.d("TAG", myResponse); // no need inside run()
                if(SharedPrefManager.getmInstance(Step1Activity.this).setKey(myResponse,"key_product")==true)
                    dialog.dismiss();
            }
        });

    }

    private boolean getInsurance(){
        Spinner insuranse=(Spinner) findViewById(R.id.insurance);
        insuranse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                summAll();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return true;
    }
    private boolean getAdultCount(){
        Spinner adult_count=(Spinner) findViewById(R.id.adult_count);
        adult_count.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                summAll();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return true;
    }

    private boolean getChildCount(){
        Spinner child_count=(Spinner) findViewById(R.id.child_count);
        child_count.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                summAll();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return true;
    }

    private boolean summInsurance(int position){

        Double summa=0.00;
        if(getCountOfChild()!=0 && getCountOfAdult()!=0)
        summa=(getTypeOfPassDuration()*getCountOfAdult())+(getTypeOfPassDuration()*getCountOfChild());
        else if(getCountOfChild()>1 && getCountOfAdult()==0)
            summa=(getTypeOfPassDuration()*getCountOfChild());
        else if(getCountOfChild()==0 && getCountOfAdult()>1)
            summa=(getTypeOfPassDuration()*getCountOfAdult());
        else
            summa=getTypeOfPassDuration();

        insurance_percent_from=(summa*insurance_percent)/100;
        insurance_total=summa+insurance_percent_from;

        if(position==1) {
            raw_insurance.setVisibility(View.VISIBLE);
            total_insurance.setText("$" + insurance_percent_from);
            total_price.setText("$" + insurance_total);
        }else{
            total_price.setText("$"+summa);
            raw_insurance.setVisibility(View.GONE);
        }

        return true;
    }

    private boolean summAll(){

        Double summa;
        Double summ_with_discount,summ_discount,summ_insurance_with_discount;
        discount=20.00;

        if(getCountOfChild()!=0 || getCountOfAdult()!=0)
            summa=(getTypeOfPassDuration()*getCountOfAdult())+(getTypeOfPassDuration()*getCountOfChild());
        else if(getCountOfChild()>1 || getCountOfAdult()==0)
            summa=(getTypeOfPassDuration()*getCountOfChild());
        else if(getCountOfChild()==0 || getCountOfAdult()>1)
            summa=(getTypeOfPassDuration()*getCountOfAdult());
        else
            summa=getTypeOfPassDuration();

        insurance_percent_from=(summa*insurance_percent)/100;
        summ_insurance_with_discount=((summa+insurance_percent_from)*discount)/100;
        insurance_total=(summa+insurance_percent_from)-summ_insurance_with_discount;
        summ_with_discount=(summa*discount)/100;
        summ_discount=summa-summ_with_discount;

        raw_discount.setVisibility(View.VISIBLE);
        total_discount.setText("$"+new DecimalFormat("0.00").format(summ_with_discount));
        total_price.setText("$"+new DecimalFormat("0.00").format(summ_discount));

        if(getPositionOfInsure()==1) {
            raw_insurance.setVisibility(View.VISIBLE);
            total_discount.setText("$"+new DecimalFormat("0.00").format(summ_insurance_with_discount));
            total_insurance.setText("$" + new DecimalFormat("0.00").format(insurance_percent_from));
            total_price.setText("$" + new DecimalFormat("0.00").format(insurance_total));
        }else{

            raw_insurance.setVisibility(View.GONE);
        }

        return true;
    }



    private boolean getSummAll(){
         //Add try catch block if necessary


        return true;
    }

    private double getTypeOfPassDuration(){
        Double pass=Double.parseDouble(pass_duration.getText().toString());
        //Log.d("PassDuaration", String.valueOf(pass));
        return pass;
    }

    private int getCountOfAdult(){
        int ad = Integer.parseInt(adult.getSelectedItem().toString());
        //Log.d("countAdult", String.valueOf(ad));
        return ad;
    }

    private int getCountOfChild(){
        int ad = Integer.parseInt(child.getSelectedItem().toString());
        //Log.d("countChild", String.valueOf(ad));
        return ad;
    }

    private int getPositionOfInsure(){
        int ad = insure.getSelectedItemPosition();
        //Log.d("PositionOfInsure", String.valueOf(ad));
        return ad;
    }

    @Override
    public void buttonClicked() {
        Toast.makeText(getApplicationContext(),"text",Toast.LENGTH_SHORT);
    }
}
