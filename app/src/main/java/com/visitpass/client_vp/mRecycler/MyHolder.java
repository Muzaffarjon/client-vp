package com.visitpass.client_vp.mRecycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.visitpass.client_vp.R;


public class MyHolder extends RecyclerView.ViewHolder {
    TextView title,adult_price,child_price;
    public MyHolder(@NonNull View itemView) {
        super(itemView);
        title=(TextView) itemView.findViewById(R.id.title);
        adult_price=(TextView) itemView.findViewById(R.id.adult_price);
        child_price=(TextView) itemView.findViewById(R.id.child_price);
    }
}
