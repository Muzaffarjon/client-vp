package com.visitpass.client_vp.mRecycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.visitpass.client_vp.R;

public class MyAdapter extends RecyclerView.Adapter<MyHolder> {

    Context c;
    String[] tvshows;
    String[] adult;
    String[] child;

    public MyAdapter(Context c, String[] tvshows,String[] adult, String[] child) {
        this.c = c;
        this.tvshows = tvshows;
        this.adult = adult;
        this.child = child;
    }

    //INITIALIE VH
    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row,parent,false);
        MyHolder holder=new MyHolder(v);
        return holder;
    }

    //BIND DATA
    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.title.setText(tvshows[position]);
        holder.adult_price.append(adult[position]);
        holder.child_price.append(child[position]);
    }

    @Override
    public int getItemCount() {
        return tvshows.length;
    }
}