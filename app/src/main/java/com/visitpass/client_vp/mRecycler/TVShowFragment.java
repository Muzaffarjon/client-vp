package com.visitpass.client_vp.mRecycler;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.visitpass.client_vp.Config;
import com.visitpass.client_vp.R;
import com.visitpass.client_vp.RecyclerItemClickListener;
import com.visitpass.client_vp.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TVShowFragment extends DialogFragment {

    String[] tvshows={"Crisis", "Blindspot","BlackList", "Game of Thrones","Gotham","Banshee"};
    String[] adult_price={"45.25", "42.35","45.33", "66.22","45.22","88.22"};
    String[] child_price={"12.12", "13.25","15.25", "46.12","48.12","45.22"};
    RecyclerView rv;
    MyAdapter adapter;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.fraglayout,container);

        //RECYCER
        rv= (RecyclerView) rootView.findViewById(R.id.mRecyerId);
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        //ADAPTER
        adapter=new MyAdapter(this.getActivity(),tvshows,adult_price,child_price);
        rv.setAdapter(adapter);

        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), rv, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Toast.makeText(getContext(), "Clicked adult: "+adult_price[position] + ", child: "+child_price[position], Toast.LENGTH_SHORT).show();
                        Log.d("dasdasd", String.valueOf(Config.getInfoFromProduct(getContext(),"name",1)));
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return rootView;
    }

}
