package com.visitpass.client_vp;

public class CategoryAlbum{



    private  int id;
    private String thumbnail;

    private String name;


    public CategoryAlbum() {
    }

    public CategoryAlbum(int id, String thumbnail,String name) {
        this.thumbnail = thumbnail;
        this.id=id;
        this.name=name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
    return name;
    }
    public int getId() {
        return id;
    }
}
