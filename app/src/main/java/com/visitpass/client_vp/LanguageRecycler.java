package com.visitpass.client_vp;

import android.support.annotation.Nullable;

public class LanguageRecycler {

    private String icon;
    private String name;
    private String tag;
    private String alias;
    private int id;

    public LanguageRecycler(){

    }

    public LanguageRecycler(String name,String icon,String tag){
        this.icon=icon;
        this.name=name;
        if(tag!=null) this.tag=tag;
    }
    public LanguageRecycler(String name,String alias,@Nullable int id){
        this.name=name;
        this.alias=alias;
            this.id=id;
    }

    public String getIcon(){
        return this.icon;
    }

    public String getName(){
        return this.name;
    }

    public String getTag(){
        return this.tag;
    }

    public String getAlias(){
        return this.alias;
    }

    public int getId(){
        return this.id;
    }

}
