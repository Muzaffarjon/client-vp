package com.visitpass.client_vp;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class SelectPriceAdapter extends RecyclerView.Adapter<SelectPriceAdapter.ViewHolder>{
    private List<SelectPrice> mData;
    private List<String> icons;
    private Context context;
    private LayoutInflater mInflater;
    private LanguageAdapter.ItemClickListener mClickListener;
    ProgressDialog dialog;
    String cities;

    // data is passed into the constructor
    SelectPriceAdapter(Context context,List<SelectPrice> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public SelectPriceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_row, parent, false);
        return new SelectPriceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SelectPrice city = mData.get(position);

        holder.title.setText(city.getTitle());
        holder.adult_price.append(city.getAdult());
        holder.child_price.append(city.getChild());

    }


    public int getItemCount() {
        return mData.size();
    }

    public float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title,adult_price,child_price;
        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            adult_price = itemView.findViewById(R.id.adult_price);
            child_price = itemView.findViewById(R.id.child_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int pos = getAdapterPosition();


        }
    }
    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
