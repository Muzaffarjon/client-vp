package com.visitpass.client_vp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SplashActivity extends AppCompatActivity {

    ProgressDialog dialog;
    public String TAG ="MemCachBoom";
    public String http="http://admin.visitpass.com/mobile/getData";
    //public String http="https://google.com";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // dialog.show();
        sendHttpRequest(1,"en","");
        //Log.d("ddasd","dasdasd");
        final Handler handler = new Handler();
       handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                finish();
                startActivity(intent);
           }
        }, 2000);
    }


    private boolean sendHttpRequest(int city_id,String lang_id,String upd){

        OkHttpClient client = new OkHttpClient();


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("city_id", String.valueOf(city_id))
                .addFormDataPart("lang_id", lang_id)
                .addFormDataPart("upd", upd)
                .build();

        Request request = new Request.Builder().url(http).post(requestBody).build();

        client.newCall(request).enqueue(new Callback() {
            String myResponse;
            @Override
            public void onFailure(Call call, IOException e) {

                Log.d(TAG, e.toString()); // no need inside run()

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                myResponse = response.body().string();
                //Log.d(TAG, myResponse); // no need inside run()
                if(!getErrorFromRequest(myResponse)){

                    getJsonHandler(myResponse);
                } // must be inside run()

            }
        });
        return  SharedPrefManager.getmInstance(this).getKey("key_attractions")==null ? false : true;

    }

    private void getJsonHandler(String myResponse) {
        getAttractions(myResponse,"attractions","key_attractions");
        getAttractions(myResponse,"cities","key_city");
        getAttractions(myResponse,"lang","key_language");
        getAttractions(myResponse,"categories","key_category");
        //getAttractions(myResponse,"upd","key_update");
    }

    private void getAttractions(String response,String keygen, String key) {

        try {
            JSONObject obj = new JSONObject(response);
            JSONArray object = obj.getJSONArray(keygen);

            SharedPrefManager.getmInstance(this).setKey(String.valueOf(object),key);
            //Log.d("IsSet"+keygen, String.valueOf(object));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean getErrorFromRequest(String myResponse) {
        //Log.d("RTERASDas","DASSSADASDASDASDSADASD");
        return false;

    }

}