package com.visitpass.client_vp;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class AttractionActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AlbumAttractionsAdapter adapter;
    public ArrayList<AlbumAttractions> dataItems;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction);
        //int sessionId= getIntent().getIntExtra("EXTRA_CATEGORY_ID",0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle(SharedPrefManager.getmInstance(this).getTitle());

        //Log.d("EXTRA_CATEGORY_ID", SharedPrefManager.getmInstance(this).getKey("key_update"));

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        if(SharedPrefManager.getmInstance(this).getKey("key_attractions")!=null) {
            dataItems = (ArrayList<AlbumAttractions>) Config.parseJsonAttractions(AttractionActivity.this, Integer.parseInt(SharedPrefManager.getmInstance(this).getKey("key_update")));

            adapter = new AlbumAttractionsAdapter(this, dataItems);
            adapter.setHasStableIds(true);
            if (this.getResources().getConfiguration().orientation ==
                    this.getResources().getConfiguration()
                            .ORIENTATION_LANDSCAPE) {
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(5), true));
            }else{
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));
            }
            //recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setHasFixedSize(true);

            recyclerView.setItemViewCacheSize(20);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            //String attraction = SharedPrefManager.getmInstance(AttractionActivity.this).getKey("key_attractions");
                            //Log.d("fromAttractions", String.valueOf(dataItems.get(position).getAttractionId()));
                            SharedPrefManager.getmInstance(AttractionActivity.this).isSingle(
                                    dataItems.get(position).getNormallyPriceAdult(),
                                    dataItems.get(position).getNormallyPriceChild(),
                                    dataItems.get(position).getInfo(),
                                    dataItems.get(position).getName(),
                                    dataItems.get(position).getAddress(),
                                    dataItems.get(position).getMapLebel(),
                                    dataItems.get(position).getMapLat(),
                                    dataItems.get(position).getMaplng(),
                                    dataItems.get(position).getThumbnail()
                            );
                            //SharedPrefManager.getmInstance(AttractionActivity.this).setKey(String.valueOf(dataItems.get(position).getAttractionId()),"key_update_single");

                            startActivity(new Intent(AttractionActivity.this, SingleActivity.class));
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {
                            // do whatever
                        }
                    })
            );

            adapter.notifyDataSetChanged();
            //prepareAlbums();
        }


    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return false;
            }
        });
        return true;
    }

    private void filter(String text) {
        ArrayList<AlbumAttractions> filteredList = new ArrayList<>();

        for (AlbumAttractions item : dataItems) {
            if (item.getName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }

        adapter.filterList(filteredList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
