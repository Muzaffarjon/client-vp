package com.visitpass.client_vp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {
    private List<LanguageRecycler> mData;
    private List<String> icons;
    private Context context;
    private LayoutInflater mInflater;
    private LanguageAdapter.ItemClickListener mClickListener;
    ProgressDialog dialog;
    String cities;
    // data is passed into the constructor
    CityAdapter(Context context, List<LanguageRecycler> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context=context;
    }

    public CityAdapter(Context context, Object o) {

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.city_recyler_language, parent, false);
        return new CityAdapter.ViewHolder(view);
    }

    // тут пишем нащи вставные данные
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LanguageRecycler city = mData.get(position);

        holder.myTextView.setText(city.getName());
        //Берем язык с дефолтного положения
        cities=SharedPrefManager.getmInstance(context).getSelectedCity();

        if(city.getAlias().equals(cities)) {
            holder.myTextView.setEnabled(false);
            holder.myTextView.setClickable(false);
            holder.myTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_box_black_24dp, 0);
            holder.myTextView.setTextColor(Color.parseColor("#BB133E"));
            if (Build.VERSION.SDK_INT > 15) {
                holder.myTextView.setBackgroundResource(R.drawable.back_city_red);
            }
        }
        //Log.d("Daasd",city.getAlias()+", "+cities);
        //для работы с картинками
    }


    // количество строк
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.goodText);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int pos = getAdapterPosition();
            LanguageRecycler clickedDataItem = mData.get(pos);
            if(cities!=clickedDataItem.getTag()){
                dialog= new ProgressDialog(context);
                dialog.setMessage("Please wait ...");
                dialog.setCancelable(false);
                dialog.show();

                // проверяем существует ли наша позиция
                if(pos != RecyclerView.NO_POSITION){

                    SharedPrefManager.getmInstance(context).setCity(clickedDataItem.getAlias());

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // После задержки 2s = 2000ms
                            ((Activity)context).finish();

                        }
                    }, 1000);


                }
            }

        }
    }
    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
