package com.visitpass.client_vp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.Calendar;
import java.util.List;

public class SingleActivity extends AppCompatActivity {

    private MapView mapView;

    private RecyclerView recyclerView;
    private CategoryAlbumAdapter adapter;
    private List<CategoryAlbum> albumList;

    TextView title, adult_price,child_price, inform, addrss;

    ImageView imagine,mapping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle("");
        try{
            String pictures=SharedPrefManager.getmInstance(SingleActivity.this).getKey("pics_all");
            Log.e("Pictures",pictures);

        }catch (Exception ex){
            Log.e("sdcard-err2:",Log.getStackTraceString(ex));
        }

        getInfoSingle();

    }

    private boolean getInfoSingle() {
        imagine = (ImageView) findViewById(R.id.imagine);
        title = (TextView) findViewById(R.id.title);
        adult_price = (TextView) findViewById(R.id.adult_price);
        child_price = (TextView) findViewById(R.id.child_price);
        inform = (TextView) findViewById(R.id.info);
        addrss = (TextView) findViewById(R.id.address);

        /*
         * Setting info
         */
        Glide.with(this).load(SharedPrefManager.getmInstance(this).getPICS()).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.logo5).fitCenter().into(imagine);
        title.setText(SharedPrefManager.getmInstance(this).getNAME());
        adult_price.setText("$" + SharedPrefManager.getmInstance(this).getAdultPrice());
        child_price.setText("$" + SharedPrefManager.getmInstance(this).getChildPrice());
        inform.setText(SharedPrefManager.getmInstance(this).getINFO());
        addrss.setText(SharedPrefManager.getmInstance(this).getADDRESS());

        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mapping:   ;
                startActivity(new Intent(SingleActivity.this,SingleMapActivity.class));
                break;

            case R.id.imageSlider:   ;
                startActivity(new Intent(SingleActivity.this,SliderActivity.class));
                break;

        }
        return super.onOptionsItemSelected(item);

    }
}
