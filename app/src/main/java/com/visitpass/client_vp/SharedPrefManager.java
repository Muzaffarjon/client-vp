package com.visitpass.client_vp;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    /*
     * User data
     */
    private static final String SHARED_PREF_NAME = "mysharedpref12";
    private static final String SELECTED_LANGUAGE = "en";
    private static final String SELECTED_CITY = "new_york";
    private static final String EMAIL = "email";
    private static final String AUTH_TOKEN = "email";
    private static final String FIRST_NAME = "email";
    private static final String SECOND_NAME = "email";
    private static final String ADDRESS_LINE_1 = "email";
    private static final String ADDRESS_LINE_2 = "email";
    private static final String CITY = "email";
    private static final String ZIP = "73500";
    private static final String COUNTRY = "1";
    private static final String TELEPHONE = "+992928344074";

    /*
     * save data
     */
    private static final String ATTRACTIONS = "attractions";
    private static final String KEY_UPDATE = "key_update";
    private static final String KEY_UPDATE_SINGLE = "key_single";
    private static final String KEY_ATTRACTIONS = "key_attractions";
    private static final String KEY_CITY = "key_city";
    private static final String KEY_LANGUAGE = "key_language";
    private static final String KEY_CATEGORY = "key_category";

    private static final String KEY_PRODUCT = "key_product";

    private static final String TITLE = "title";

    /*
    * info for single page
    */
    private static final String ADULT_PRICE = "adult_price";
    private static final String CHILD_PRICE = "child_price";
    private static final String INFO = "info";
    private static final String NAME = "name";
    private static final String ADDRESS = "address";
    private static final String MAP_LEBEL = "map_lebel";
    private static final String MAP_LAT = "map_lat";
    private static final String MAP_LNG = "map_lng";
    private static final String PICS = "pics";
    private static final String PICS_ALL = "pics_all";


    /*
     * where we could use
     */
    private SharedPrefManager(Context context){
        mCtx=context;
    }


    public static synchronized SharedPrefManager getmInstance(Context context){
        if(mInstance==null){
            mInstance=new SharedPrefManager(context);
        }
        return mInstance;
    }


    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        if(sharedPreferences.getString(AUTH_TOKEN,null)!=null){
            return true;
        }
        return false;
    }

    public boolean setTitle(String title){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        //Log.d("DDASDASDASDSADASD",title);
        editor.putString(TITLE, title);
        editor.apply();

        return true;
    }

    public String getTitle(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(TITLE,null);
    }



    public boolean logout(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public boolean isSingle(String adult_price, String child_price,String info,String name,String address, String map_lebel, String map_lat, String map_lng, String pics){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(ADULT_PRICE, adult_price);
        editor.putString(CHILD_PRICE, child_price);
        editor.putString(INFO, info);
        editor.putString(NAME, name);
        editor.putString(ADDRESS, address);
        editor.putString(MAP_LEBEL, map_lebel);
        editor.putString(MAP_LAT, map_lat);
        editor.putString(MAP_LNG, map_lng);
        editor.putString(PICS, pics);

        editor.apply();

        return true;
    }



    // SETTERS

    public boolean userLogin(String email,String auth_token){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(EMAIL, email);
        editor.putString(AUTH_TOKEN, auth_token);
        editor.apply();

        return true;
    }

    public boolean userInfo(@Nullable String email,String first_name,String second_name, String city,String telephone,Integer zip_code,Integer country, String address_1,String address_2){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(!TextUtils.isEmpty(email))
        editor.putString(EMAIL, email);

        editor.putString(FIRST_NAME, first_name);
        editor.putString(SECOND_NAME, second_name);
        editor.putString(CITY, city);
        editor.putString(ADDRESS_LINE_1, address_1);
        editor.putString(ADDRESS_LINE_2, address_2);
        editor.putString(TELEPHONE, telephone);
        editor.putInt(ZIP,zip_code);
        editor.putInt(COUNTRY,country);

        editor.apply();

        return true;
    }



    public boolean setLanguage(String language){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(SELECTED_LANGUAGE,language);
        editor.apply();

        return true;
    }

    public boolean setKey(String data,String key){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //Log.d("Tadsdasd",key);
        editor.putString(key,data);
        editor.apply();

        return true;
    }

    public String getKey(String key){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,null);
    }

    public boolean setCity(String city){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(SELECTED_CITY,city);
        editor.apply();

        return true;
    }

    public void clearData(String key){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(key).commit();
    }

    /* GETTERS */

    public String getSelectedLanguage(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(SELECTED_LANGUAGE,null);
    }

    public String getSelectedCity(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(SELECTED_CITY,null);
    }

    public String getFirstName(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(FIRST_NAME,null);
    }

    public String getSecondName(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(SECOND_NAME,null);
    }

    public String getEmail(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(EMAIL,null);
    }

    public String getCity(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(CITY,null);
    }

    public String getAddressLine1(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(ADDRESS_LINE_1,null);
    }

    public String getAddressLine2(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(ADDRESS_LINE_2,null);
    }

    public String getTelephone(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(TELEPHONE,null);
    }

    public String getCountry(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(COUNTRY,null);
    }

    public static String getAdultPrice() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);

        return sharedPreferences.getString(ADULT_PRICE,null);
    }

    public static String getChildPrice() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);

        return sharedPreferences.getString(CHILD_PRICE,null);
    }

    public static String getINFO() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(INFO,null);
    }

    public static String getNAME() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(NAME,null);
    }

    public static String getADDRESS() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(ADDRESS,null);
    }

    public static String getMapLebel() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(MAP_LEBEL,null);
    }

    public static String getMapLat() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(MAP_LAT,null);
    }

    public static String getMapLng() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(MAP_LNG,null);
    }

    public static String getPICS() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(PICS,null);
    }


    /* END_GETTERS */

}
