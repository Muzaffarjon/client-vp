package com.visitpass.client_vp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class BeforeActivity extends AppCompatActivity {

    private View mEmail;
    private View mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_before);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mEmail=findViewById(R.id.email_form);
        mPassword=findViewById(R.id.password_form);

        Button btn = (Button) findViewById(R.id.btn_ok);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BeforeActivity.this,Step2Activity.class));
            }
        });

        if (SharedPrefManager.getmInstance(this).isLoggedIn()==true){
            mEmail.setVisibility(View.GONE);
            mPassword.setVisibility(View.GONE);
        }
    }
}
